<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\TrackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/artist', [ArtistController::class, 'index'])->name('artist.index');
Route::get('/artist/{artist}', [ArtistController::class, 'show'])->name('artist.show');

Route::get('/album/{album}', [AlbumController::class, 'show'])->name('album.show');

Route::get('/track', [TrackController::class, 'index'])->name('track.index');
Route::get('/track/{track}', [TrackController::class, 'show'])->name('track.show');

Route::get('/playlist/{playlist}', [PlaylistController::class, 'show'])->name('playlist.show');