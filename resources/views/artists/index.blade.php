@extends('layout.layout')

@section('content')


        <h2>Artist</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach($artists as $artist)
                    <tr>
                        <td><a href="{{route('artist.show', $artist->id)}}">{{ $artist->id }}</a></td>
                        <td><a href="{{route('artist.show', $artist->id)}}">{{ $artist->name }}</a></td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>


@endsection

