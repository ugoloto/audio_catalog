@extends('layout.layout')

@section('content')

        <h2>Artist</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <tr>
                    <td scope="col">id</td>
                    <td><p>{{ $artist['id'] }}</p></td>

                </tr>

                <tr>
                    <td scope="col">Name</td>
                    <td><p>{{ $artist['name'] }}</p></td>
                </tr>
                @foreach($artist->album as $album)
                <tr>
                    <td scope="col">Album title</td>
                    <td scope="col">
                            <a href="{{route('album.show', $album->id)}}">{{ $album->title }}</a>
                    </td>
                </tr>
                @endforeach

            </table>

        </div>


@endsection
