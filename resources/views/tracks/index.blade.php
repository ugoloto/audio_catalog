@extends('layout.layout')

@section('content')
    <h2>Tracks</h2>
    <table class="table">
        <tr>
            <th>Track ID</th>
            <th>Name</th>
            <th>Composer</th>
            <th>Milliseconds</th>
            <th>Bytes</th>
            <th>Price</th>
        </tr>

        @foreach($tracks as $track)
            <tr>
                <td><p>{{ $track['id'] }}</p></td>
                <td><a href="{{route('track.show', $track->getKey())}}">{{ $track->name }}</a></td>
                <td><p>{{ $track->composer ?? 'No composer' }}</p></td>
                <td><p>{{ $track->milliseconds }}</p></td>
                <td><p>{{ $track->bytes }}</p></td>
                <td><p>{{ $track->unit_price }}</p></td>
            </tr>

        @endforeach

    </table>
    <br>
    {!! $tracks->links('pagination::bootstrap-4') !!}

@endsection
