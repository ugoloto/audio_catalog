@extends('layout.layout')

@section('content')
        <h2>{{$track->name}}</h2>

    <table border="1">
    <tr>
        <td><p>Id</p></td>
        <td><p>{{ $track->id }}</p></td>
    </tr>
    <tr>
        <td><p>Name</p></td>
        <td><p>{{ $track->name }}</p></td>
    </tr>
    <tr>
        <td><p>Composer</p></td>
        <td><p>{{ $track->composer ?? 'No composer' }}</p></td>
    </tr>
    <tr>
        <td><p>Milliseconds</p></td>
        <td><p>{{$track->time }}</p></td>
    </tr>
    <tr>
        <td><p>Bytes</p></td>
        <td><p>{{ $track->mb }}</p></td>
    </tr>
    <tr>
        <td><p>Price</p></td>
        <td><p>{{ $track->unit_price }}</p></td>
    </tr>
        <tr>
            <td><p>Genre</p></td>
            <td><p>{{ $track->genre->name }}</p></td>
        </tr>

</table>

        <div>
            <h2>Playlists</h2>
            <ul>
                @foreach($track->playlist as $playlist)
                    <li>
                        <a href="{{route('playlist.show', $playlist->getKey())}}">{{$playlist->name}}</a>

                    </li>
                @endforeach
            </ul>
        </div>
        <div>
            <h2>Artist name</h2>
            <ul>
                    <li>
                        <a href="{{route('artist.show', $track->album->artist->getKey())}}">{{$track->album->artist->name}}</a>

                    </li>
            </ul>
        </div>


@endsection

