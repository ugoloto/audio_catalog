@extends('layout.layout')

@section('content')

        <h2>Name album {{$album->title}}</h2>
        <h2>Name artist {{$album->artist->name}}</h2>
        <div class="table-responsive">
            <ul>
                @foreach($album->track as $track)
                <li>
                        <a href="{{route('track.show', $track->id)}}">{{ $track->name }}</a>
                </li>
                @endforeach
            </ul>



        </div>


@endsection
