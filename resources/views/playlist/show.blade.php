@extends('layout.layout')

@section('content')

        <h2>Name playlist {{$playlist->name}}</h2>

        <div class="table-responsive">
            <ul>
                @foreach($tracks as $track)
                <li>
                        <a href="{{route('track.show', $track->id)}}">{{ $track->name }}</a>
                </li>
                @endforeach
            </ul>
            {!! $tracks->links('pagination::bootstrap-4') !!}

        </div>


@endsection
