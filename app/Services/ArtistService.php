<?php

namespace App\Services;

use App\Models\Artist;
use Illuminate\Support\Collection;

class ArtistService
{
    public function getAllArtists(): Collection
    {
        return Artist::query()
            ->select(['id', 'name'])
            ->limit(10)
            ->get();
    }

    /**
     * @param int $id
     * @return Artist
     */
    public function getArtistById(int $id): Artist
    {
        /** @var Artist $track */
        $artist = Artist::query()
            ->select(['id', 'name'])
            ->where('id', $id)
            ->first();

        return $artist;
    }
}
