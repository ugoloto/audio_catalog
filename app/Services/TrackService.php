<?php

namespace App\Services;

use App\Models\Track;
use Illuminate\Support\Collection;

class TrackService
{
    public function getAllTracks(): Collection
    {
        return Track::query()
            ->select(['id', 'name', 'composer', 'milliseconds', 'bytes', 'unit_price'])
            ->limit(10)
            ->get();
    }

    /**
     * @param int $id
     * @return Track
     */
    public function getTrackById(int $id): Track
    {
        /** @var Track $track */
        $track = Track::query()
            ->select(['id', 'name', 'composer', 'milliseconds', 'bytes', 'unit_price'])
            ->where('id', $id)
            ->first();

        return $track;
    }
}
