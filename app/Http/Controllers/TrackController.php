<?php

namespace App\Http\Controllers;

use App\Models\Track;
use Illuminate\Http\Request;
use App\Services\TrackService;
use Illuminate\View\View;

class TrackController extends Controller
{
    public function index(): View
    {
        $tracks = Track::paginate(25);
        return view('tracks.index')->with(['tracks' => $tracks]);
    }

    public function show(Track $track): View
    {
        $track->load('playlist', 'genre', 'album');
        return view('tracks.show')->with(['track' => $track]);
    }
}
