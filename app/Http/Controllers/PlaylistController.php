<?php

namespace App\Http\Controllers;

use App\Models\Playlist;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PlaylistController extends Controller
{
    public function show(Playlist $playlist): View
    {
        $tracks = $playlist->track()->paginate(25);
        return view('playlist.show',['playlist' => $playlist, 'tracks' => $tracks] );
    }
}
