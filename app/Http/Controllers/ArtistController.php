<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use Illuminate\Http\Request;
use App\Services\ArtistService;
use Illuminate\View\View;

class ArtistController extends Controller
{
    public function index()
    {
        $artists = Artist::all();

        return view('artists.index')->with(['artists' => $artists]);
    }

    /**
     * @param Artist $artist
     * @return View
     */
    public function show(Artist $artist): View
    {
        $artist->load('album');
        return view('artists.show')->with(['artist' => $artist]);
    }
}
