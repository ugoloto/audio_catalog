<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Track extends Model
{
    use HasFactory;
    public function genre(): BelongsTo
    {
        return $this->belongsTo(Genre::class);
    }
    public function playlist(): BelongsToMany
    {
        return $this->belongsToMany(Playlist::class);
    }

    public function album(): BelongsTo
    {
        return $this->belongsTo(Album::class);
    }

    public function getMbAttribute()
    {
        return round($this->bytes /1024 /1024, 2);
    }

    public function getTimeAttribute()
    {
        return date('i:s', $this->milliseconds / 1000);
    }
}
